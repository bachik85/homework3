

let page = document.querySelector('.page');
let themeButton = document.getElementById('button');

themeButton.onclick = function () {
    page.classList.toggle('main-theme');
    page.classList.toggle('grey-theme');
    localStorage.setItem('theme', 'grey');
}

